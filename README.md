# VIM CLI
[Cheetsheet](https://www.keycdn.com/blog/vim-commands)

## Basic

* :w - Saves the file you are working on
* :wq - Save your file and close Vim
* :q! - Quit without first saving the file you were working on

## For movement

* h - Moves the cursor to the left
* l - Moves the cursor to the right
* j - Moves the cursor down one line
* k - Moves the cursor up one line
* H - Puts the cursor at the top of the screen
* M - Puts the cursor in the middle of the screen
* L - Puts the cursor at the bottom of the screen
* w - Puts the cursor at the start of the next word
* b - Puts the cursor at the start of the previous word
* e - Puts the cursor at the end of a word
* 0 - Places the cursor at the beginning of a line
* $ - Places the cursor at the end of a line
* ) - Takes you to the start of the next sentence
* ( - Takes you to the start of the previous sentence
* } - Takes you to the start of the next paragraph or block of text
* { - Takes you to the start of the previous paragraph or block of text
* Ctrl+f - Takes you one page forward
* Ctrl+b - Takes you one page back
* gg - Places the cursor at the start of the file
* G - Places the cursor at the end of the file
* `#` - Where # is the number of a line, this command takes you to the line specified

## For editing

* yy - Copies a line
* yw - Copies a word
* y$ - Copies from where your cursor is to the end of a line
* v - Highlight one character at a time using arrow buttons or the h, k, j, l buttons
* V - Highlights one line, and movement keys can allow you to highlight additional lines
* p - Paste whatever has been copied to the unnamed register
* d - Deletes highlighted text
* dd - Deletes a line of text
* dw - Deletes a word
* D - Deletes everything from where your cursor is to the end of the line
* d0 - Deletes everything from where your cursor is to the beginning of the line
* dgg - Deletes everything from where your cursor is to the beginning of the file
* dG - Deletes everything from where your cursor is to the end of the file
* x - Deletes a single character
* u - Undo the last operation; u# allows you to undo multiple actions
* Ctrl+r - Redo the last undo
* . - Repeats the last action

## Important use cases

1. [Comment and uncomment lines](https://stackoverflow.com/questions/1676632/whats-a-quick-way-to-comment-uncomment-lines-in-vim)
